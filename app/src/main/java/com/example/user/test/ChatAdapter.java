package com.example.user.test;

/**
 * Created by USER on 21-05-2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    private TextView chatText;
    private LinearLayout singleMessageContainer;
    String chatMessage;
    boolean side;
    ArrayList<ChatMessage> chatMessageList = new ArrayList<>();


    public ChatAdapter(Context context, int textViewResourceId, ArrayList<ChatMessage> chatMessageList) {
        super(context, textViewResourceId);
        this.chatMessageList = chatMessageList;

    }

    @Override
    public void add(ChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }


    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.message_row, parent, false);
        }


        singleMessageContainer = (LinearLayout) row.findViewById(R.id.chat_layout);
        chatText = (TextView) row.findViewById(R.id.chat_mssg);


        ChatMessage chatMessageObj = getItem(position);
        side = chatMessageObj.left;
        chatMessage = chatMessageObj.message;


        if (side == true) {
            singleMessageContainer.setGravity(Gravity.RIGHT);
            chatText.setBackgroundColor(Color.argb(255, 170, 230, 249));

        } else {
            singleMessageContainer.setGravity(Gravity.LEFT);
            chatText.setBackgroundColor(Color.argb(0,0,0,0));
        }
        Log.i("Adapter", chatMessage + " " + side);


        chatText.setText(chatMessage);


        return row;
    }


}