package com.example.user.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 24-05-2017.
 */

public class Test extends AppCompatActivity{

   View vv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.test);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        vv=findViewById(R.id.myview);

        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbarLayout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Dr.Bot");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });

//        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
//        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);



        Toast.makeText(this, "Scroll up the Image to see collapsing effect.", Toast.LENGTH_LONG).show();


    }

    public void goto_chat(View v) {
        startActivity(new Intent(Test.this, IntroChatActivity.class));
    }


    public void askdoc(View v){
        Snackbar.make(vv,"Asked a doctor",Snackbar.LENGTH_SHORT).show();

    }

    public void ordermed(View v){
        Snackbar.make(vv,"Ordered a medicine",Snackbar.LENGTH_SHORT).show();

    }

    public void bloodtest(View v){
        Snackbar.make(vv,"Blood Tested",Snackbar.LENGTH_SHORT).show();

    }

    public void bookscan(View v){
        Snackbar.make(vv,"Scan Booked",Snackbar.LENGTH_SHORT).show();

    }

    public void notification(View v){
        Snackbar.make(vv,"No notifications",Snackbar.LENGTH_SHORT).show();

    }

    public void hospital(View v){
        Snackbar.make(vv,"No Hospitals",Snackbar.LENGTH_SHORT).show();

    }

    public void ambulance(View v){
        Snackbar.make(vv,"No Ambulance",Snackbar.LENGTH_SHORT).show();

    }
    public void settings(View v){
        Snackbar.make(vv,"No Settings",Snackbar.LENGTH_SHORT).show();

    }

}
