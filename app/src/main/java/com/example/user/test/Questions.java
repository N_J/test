package com.example.user.test;

/**
 * Created by USER on 13-06-2017.
 */
public class Questions {

    public ChatMessage qintro = new ChatMessage(false, "Hi " + " Rakhi" + ", Welcome to DR.Bot");
    public ChatMessage typ = new ChatMessage(false, "Typing...");

    public ChatMessage q0 = new ChatMessage(false, "Who is the patient?\n" +
            "a. Myself\nb. Others");

    public ChatMessage q1 = new ChatMessage(false, "What is the Patients age?");

    public ChatMessage q2 = new ChatMessage(false, "What is the patient gender?\na. Male\nb. Female");

    public ChatMessage q3 = new ChatMessage(false, "What is the relation of patient with you?");

    public ChatMessage endchat = new ChatMessage(false, "No more questions, Thank you!!");

    public ChatMessage error = new ChatMessage(false, "Please submit a valid option!");

    public ChatMessage q4= new ChatMessage(false, "What problems is the patient facing?");

    public ChatMessage q5 = new ChatMessage(false, "Since when have the patient had this problem?\n MM/YYYY format");

    public ChatMessage okay = new ChatMessage(false, "Okay! Got it.");

    public ChatMessage q6 = new ChatMessage(false, "Does the patient have fever?\na. Yes\nb. No");

    public ChatMessage q7 = new ChatMessage(false, "Give a brief description of the problems faces by patient.");

    public ChatMessage q8 = new ChatMessage(false, "Dr. Rio will take up your case. Let him study your case and we will reply back.");


    public ChatMessage invalid_date = new ChatMessage(false, "Invalid date! Please try again.");

    public ChatMessage pleasure = new ChatMessage(false, "It was a pleasure talking to you Rakhi.");

    public ChatMessage dontwry = new ChatMessage(false, "Please don't worry");


    public ChatMessage okay3 = new ChatMessage(false, "Noted!.");
    public ChatMessage okay6 = new ChatMessage(false, "Alright.");
}
